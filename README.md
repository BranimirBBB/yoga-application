# Yoga application

In semester 4 I worked with Mr. M.Tomov who is a friend of mine and also a classmate. 

Our project was to build a Sport application with working PWA. 

We choosed to create a Yoga Application for yoga at home.

We chose this topic based on a persona that our teachers gave us beforehand.

Our project was one of the best in our class and the teachers were very happy with our effort.

More about the project you can find the Project Guide that I made.

See how it is looking:

[Live Demo](https://incomparable-cupcake-856620.netlify.app)
